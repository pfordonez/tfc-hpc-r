# 8628 archivos con un total de 44,6 MB
### Really starting

library(dplyr)
# Read all files in directory
setwd('/home/miltonlab/devs/inamhi/')
# try files with size zero
# Result one list of data.frames
data <- lapply(list.files(path = 'm5147/', full.names = TRUE ), 
               function(f) {
                 if (!file.size(f)==0) 
                   read.csv(f, colClasses=c("fecha"="character")) %>% mutate(sensor = f) 
               })

# Only one dataframe
data2 <- do.call(rbind, data)

# Set correct date type
data2$fecha = as.POSIXct(data2$fecha, format = "%Y%m%d%H%M%S")

library(lubridate) # para manejo de fechas y horas
# Consulta de gabo
# Promedio de la temperatura por hora entre los días 10 - 15
temp_by_hour <- data2 %>% filter(day(fecha) %in% c(10:15)) %>% group_by(hour(fecha)) %>% summarise("TEMP_BY_HOUR" = mean(X1_29341m))

# Promedio de la temperatura por dia 
temp_by_day <- data2 %>% group_by(day(fecha)) %>% summarise("TEMP_BY_DAY" = mean(X1_29341m))

##test <- lapply(data[1], function(df) { df$fecha <- as.POSIXct(df$fecha, format = "%Y%m%d%H%M%S")})
#ERROR:
#  Error in as.POSIXct.default(data[[i]]$fecha, format = "%Y%m%d%H%M%S") : do not know how to convert #data[[i]]$fecha' to class “POSIXct”

